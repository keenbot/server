pip freeze > config/requirements.dev.txt

# conda env export | grep -v "^prefix: " > environment.yml

# See https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#building-identical-conda-environments
conda list --explicit > config/spec-file.dev.txt