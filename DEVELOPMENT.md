[DBDiagram](https://dbdiagram.io/d/5e8bee593d30f00839b39bf7)


## Forcing a new deploy without adding a commit to Github repository
   [Reference](https://www.darraghoriordan.com/2019/03/02/heroku-push-failed-force-rebuild/)

1. Install the following plugin.
   ```
   heroku plugins:install heroku-releases-retry
   ```

2. Execute the following command.
   ```
   heroku releases:retry
   ```


## function uuid_generate_v4() does not exist

If you get the following error stating that `uuid_generate_v4()` does not exist in the PostgreSQL database:
```
sqlalchemy.exc.ProgrammingError: (psycopg2.errors.UndefinedFunction) function uuid_generate_v4() does not exist
```

Follow these steps:
1. Access Heroku's PostgreSQL database terminal.
   ```
   heroku pg:psql
   ```

2. Enter the following command into the database terminal.
   ```
   CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
   ```
