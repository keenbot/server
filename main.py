from server.app import app
from dotenv import load_dotenv
import os

load_dotenv()

if __name__ == "__main__":
    debug = os.getenv("ENV") == "dev"
    app.run(debug=debug)
