from werkzeug.wrappers import BaseRequest
from ..utils import create_response


class DataValidatorMiddleware:
    _allowed_methods = ["POST", "PUT", "DELETE"]

    messages = {
        "is_not_json": "Please be sure to use \"Content-Type\": \"application/json\" in your header."
    }

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    def __init__(self, app):
        self.app = app

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    def __call__(self, environ, start_response):
        request = BaseRequest(environ, shallow=True)

        # Checks if the data is formatted as JSON.
        # if request.method in self._allowed_methods:

        return self.app(environ, start_response)

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    def is_not_json(self, request):
        """
        Checks if the request body is formatted as JSON.
        """
        if not request.is_json:
            return create_response(415, message=self.messages["is_not_json"])
        else:
            return None
