from flask import Response
import json


"""
Defines the default HTTP status code messages.
"""
messages = {
    200: "OK",
    201: "Created",
    202: "Accepted",
    204: "No Content",
    400: "Bad Request",
    401: "Unauthorized",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    408: "Request Timeout",
    415: "Unsupported Media Type",
    422: "Unprocessable Entity",
    429: "Too Many Requests",
    500: "Internal Server Error",
    503: "Service Unavailable"
}

"""
References the valid status codes that can be passed as an argument for
`status` in `create_response()`.
"""
valid_status_codes = tuple(messages.keys())

"""
References the valid body types that can be passed as an argument for `body` in
`create_response()`.
"""
valid_body_types = (type(None), dict, list)


def create_response(status=200, message=None, body=None):
    """
    Creates a response object to send to the client.


    Parameters
    ----------
    status (int):
    HTTP status code

    message (string=None):
    Message to send to the client

    body (any=None):
    Data to send to the client
    """

    # Raises a `TypeError` if the `status` argument is not an integer.
    if not type(status) is int:
        raise TypeError(
            f"The \"status\" argument must be one of the following integers: {valid_status_codes}.")

    # Raises a `ValueError` if the `status` argument is not a key in the
    # `messages` dictionary.
    if not status in valid_status_codes:
        raise ValueError(
            f"The \"status\" argument must be one of the following integers: {valid_status_codes}.")

    # Raises a `TypeError` if the `message` argument is not None or a string.
    if not message is None and not type(message) is str:
        raise TypeError("The \"message\" argument must be of type string.")

    # Raises a `TypeError` if the `body` argument is not a NoneType, list, or a
    # dictionary.
    if not type(body) in valid_body_types:
        raise TypeError(
            f"The \"body\" argument must be one of the following types: {valid_body_types}.")

    response = {
        "status": status,
        "message": messages[status]
    }

    if message != None:
        response["message"] += f": {message}"

    if body != None:
        response["body"] = body

    return Response(
        json.dumps(response),
        status=status,
        mimetype="application/json")
