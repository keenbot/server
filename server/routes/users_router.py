from flask import Blueprint, request
from ..database.functions import DatabaseFunctions
from ..database.models import User
from ..utils import create_response


users_router = Blueprint("users", __name__)


@users_router.route("", methods=["GET"])
def get():
    """
    Retrieves user records from the database.
    """
    return DatabaseFunctions.get(User)


@users_router.route("", methods=["POST"])
def post():
    """
    Creates a new user record in the database.
    """
    return DatabaseFunctions.create(model=User, data=request.json)


@users_router.route("<user_id>", methods=["PUT"])
def update(user_id=None):
    """
    Updates the database record for the user with the specified `id`.


    Parameters
    ----------
    user_id (string):
    User id
    """

    if user_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.update(model=User, id=user_id, data=request.json)


@users_router.route("<user_id>", methods=["DELETE"])
def delete(user_id=None):
    """
    Deletes the database record for the user with the specified `id`.


    Parameters
    ----------
    user_id (string):
    User id
    """

    if user_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.delete(model=User, id=user_id)
