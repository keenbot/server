from flask import Blueprint, request
from ..database.functions import DatabaseFunctions
from ..database.models import IntentPattern, Intent
from ..utils import create_response


patterns_router = Blueprint("patterns", __name__)


@patterns_router.route("", methods=["GET"])
def get():
    """
    Retrieves pattern records from the database.
    """
    return DatabaseFunctions.get(IntentPattern)


@patterns_router.route("", methods=["POST"])
def post():
    """
    Creates a new pattern record in the database.
    """
    intent_id = request.json.pop("intent_id", None)

    return DatabaseFunctions.create_one_to_many(
        target_model=IntentPattern,
        relationship_model=Intent,
        foreign_key=intent_id,
        back_populate_list="patterns",
        data=request.json
    )


@patterns_router.route("<pattern_id>", methods=["PUT"])
def update(pattern_id=None):
    """
    Updates the database record for the pattern with the specified `id`.


    Parameters
    ----------
    pattern_id (string):
    Pattern id
    """

    if pattern_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.update(
        model=IntentPattern,
        id=pattern_id,
        data=request.json)


@patterns_router.route("<pattern_id>", methods=["DELETE"])
def delete(pattern_id=None):
    """
    Deletes the database record for the pattern with the specified `id`.


    Parameters
    ----------
    pattern_id (string):
    Pattern id
    """

    if oattern_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.delete(model=IntentPattern, id=pattern_id)
