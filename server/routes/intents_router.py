from flask import Blueprint, request
from ..database.functions import DatabaseFunctions
from ..database.models import Intent, User
from ..utils import create_response


intents_router = Blueprint("intents", __name__)


@intents_router.route("", methods=["GET"])
def get():
    """
    Retrieves intent records from the database.
    """
    return DatabaseFunctions.get(Intent)


@intents_router.route("", methods=["POST"])
def post():
    """
    Creates a new intent record in the database.
    """
    user_id = request.json.pop("user_id", None)

    return DatabaseFunctions.create_one_to_many(
        target_model=Intent,
        relationship_model=User,
        foreign_key=user_id,
        back_populate_list="intents",
        data=request.json
    )


@intents_router.route("<intent_id>", methods=["PUT"])
def update(intent_id=None):
    """
    Updates the database record for the intent with the specified `id`.


    Parameters
    ----------
    intent_id (string):
    intent id
    """

    if intent_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.update(model=Intent, id=intent_id, data=request.json)


@intents_router.route("<intent_id>", methods=["DELETE"])
def delete(intent_id=None):
    """
    Deletes the database record for the intent with the specified `id`.


    Parameters
    ----------
    intent_id (string):
    intent id
    """

    if intent_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.delete(model=Intent, id=intent_id)
