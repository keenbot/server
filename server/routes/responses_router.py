from flask import Blueprint, request
from ..database.functions import DatabaseFunctions
from ..database.models import IntentResponse, Intent
from ..utils import create_response


responses_router = Blueprint("responses", __name__)


@responses_router.route("", methods=["GET"])
def get():
    """
    Retrieves response records from the database.
    """
    return DatabaseFunctions.get(IntentResponse)


@responses_router.route("", methods=["POST"])
def post():
    """
    Creates a new response record in the database.
    """

    intent_id = request.json.pop("intent_id", None)

    return DatabaseFunctions.create_one_to_many(
        target_model=IntentResponse,
        relationship_model=Intent,
        foreign_key=intent_id,
        back_populate_list="responses",
        data=request.json
    )


@responses_router.route("<response_id>", methods=["PUT"])
def update(response_id=None):
    """
    Updates the database record for the response with the specified `id`.


    Parameters
    ----------
    response_id (string):
    Response id
    """

    if response_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.update(
        model=IntentResponse,
        id=response_id,
        data=request.json)


@responses_router.route("<response_id>", methods=["DELETE"])
def delete(response_id=None):
    """
    Deletes the database record for the response with the specified `id`.


    Parameters
    ----------
    response_id (string):
    Response id
    """

    if oattern_id == None:
        return create_response(status=422, message="No id specified.")

    return DatabaseFunctions.delete(model=IntentResponse, id=response_id)
