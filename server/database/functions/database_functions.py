from sqlalchemy.exc import DataError, IntegrityError
from ..models.mixins import ColumnValidatorMixin
from ..session import Session
from ...utils import create_response
from uuid import UUID


class DatabaseFunctions:
    @staticmethod
    def get(model):
        """
        Retrieves all database records for the specified model.


        Parameters
        ----------
        model ():
        SQLAlchemy model


        Returns
        -------
        (flask.Response):
        Response containing a dictionary with the following:
        {
            "status": 200,
            "message": "OK",
            "body": [<model_instance>, <model_instance>] or []
        }
        """
        current_session = Session()
        records = current_session.query(model).all()

        records = [record.get_dict() for record in records]
        return create_response(status=200, message=None, body=records)

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    @staticmethod
    def create(model, data):
        """
        Creates a new database record for the specified model with the given
        data.

        Todo
        ----
        - Perform a check if all records have `get_dict()` method.


        Parameters
        ----------
        model ():
        SQLAlchemy model

        data (dict):
        Data for the creation of the new instance
        """

        if not issubclass(model, (ColumnValidatorMixin)):
            """
            Checks if the given SQLAlchemy model class is a child class of all
            parent classes listed in the tuple.
            """
            raise ValueError(
                "Expected model to be a child class of ColumnValidatorMixin")

        if not model.can_create(instance=data):
            return create_response(status=422)

        try:
            """
            Creates a new record in the database.
            """
            current_session = Session()
            record = model(**data)
            current_session.add(record)
            current_session.commit()

            return create_response(
                status=201,
                message=None,
                body=record.get_dict())

        except IntegrityError:
            """
            Handles the following exceptions:
            - A record with a unique value already exists
            """
            return create_response(status=422)

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    @staticmethod
    def update(model, id, data):
        """
        Updates the database record for the specified model with the given `id`.


        Parameters
        ----------
        model ():
        SQLAlchemy model

        id (string):
        Record id
        """
        try:
            current_session = Session()
            record = current_session.query(model).filter(
                model.id == UUID(id)).update(data)

            record = current_session.query(model).filter(
                model.id == UUID(id)).first().get_dict()

            current_session.commit()

            return create_response(body=record)

        except:
            return create_response(status=422)

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    @staticmethod
    def delete(model, id):
        """
        Deletes the database record for the specified model with the given `id`.


        Parameters
        ----------
        model ():
        SQLAlchemy model

        id (string):
        Record id
        """

        try:
            """
            Attempts to delete the database record with the specified `id` as a
            UUID.
            """
            current_session = Session()
            deleted_rows = current_session.query(model).filter(
                model.id == UUID(id)).delete()

            Session.commit()

            if not deleted_rows:
                """
                Handles the case where the `id` is a valid UUID but no record with the given `id` was found.
                """
                return create_response(status=204)

            else:
                return create_response(status=200)

        except ValueError:
            """
            Handles the following exceptions:
            - `id` parameter is not a valid UUID string
            """
            return create_response(status=415)

        except:
            return create_response(status=422)

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    @staticmethod
    def create_one_to_many(
            target_model,        # Model to create the new record
            relationship_model,  # Model to associate the foreign key to
            foreign_key,         # Foreign key
            back_populate_list,  # Attribute name of the list to back populate
            data):               # Data to provide the database

        current_session = Session()

        relationship_record = current_session.query(relationship_model).filter(
            relationship_model.id == UUID(foreign_key)).first()

        record = target_model(**data)

        getattr(relationship_record, back_populate_list).append(record)
        current_session.add(record)
        current_session.commit()

        return create_response(
            status=201,
            message=None,
            body=record.get_dict())
