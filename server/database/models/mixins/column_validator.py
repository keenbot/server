class ColumnValidatorMixin:
    """
    Defines a SQLAlchemy model mixin that compares and validates keys from the
    model to outside dictionaries.


    Example
    -------
    class MySQLAlchemyModel(ColumnValidatorMixin):
        all_columns = ["a", "b", "c"]
        required_columns_for_create = ["a"]

    my_sqlalchemy_instance.has_exact_columns({"a": 1, "b": 2, "c": 3})
    # Returns True

    my_sqlalchemy_instance.has_exact_columns({"a": 1, "b": 2, "c": 3, "d": 4})
    # Returns False

    my_sqlalchemy_instance.has_exact_columns({"a": 1, "b": 2})
    # Returns False

    my_sqlalchemy_instance.has_exact_columns(["a", "b", "c"])
    # Raises ValueError


    Attributes
    ----------
    all_columns (list):
    List of all of the columns for the SQLAlchemy model

    required_columns_for_create (list):
    List of the required columns necessary for the database to successfully
    create a new record
    """

    all_columns = []

    required_columns_for_create = []

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    @classmethod
    def has_exact_columns(self, instance):
        """
        Performs a check on the keys of the given instance to ensure that the instance is neither missing keys or has more keys than expected.


        Parameters
        ----------
        instance (dict):
        Database record represented as a dictionary


        Returns
        -------
        (bool):
        Returns `True` if the given instance's keys are exact
        """

        if not isinstance(instance, dict):
            raise ValueError(
                f"Expected \"instance\" to be of type <class 'dict'>. Instead got type {type(instance)}")

        if len(self.all_columns) != len(instance.keys()):
            return False

        for col in self.all_columns:
            if col not in instance:
                return False

        return True

    # --------------------------------------------------------------------------

    # --------------------------------------------------------------------------

    @classmethod
    def can_create(self, instance):
        """
        Determines whether or not the specified instance can be created by the
        SQLAlchemy model class.


        Parameters
        ----------
        instance (dict):
        Dictionary containing the data to be passed into the database


        Returns
        -------
        (boolean):
        If `True`, the instance can be created by the SQLAlchemy model.

        If `False`, the instance is either missing keys or has too many keys
        than what is required.
        """

        if not isinstance(instance, dict):
            raise ValueError(
                f"Expected \"instance\" to be of type <class 'dict'>. Instead got type {type(instance)}")

        if len(self.required_columns_for_create) != len(instance.keys()):
            return False

        for col in self.required_columns_for_create:
            if col not in instance:
                return False

        return True
