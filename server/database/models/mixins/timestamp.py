from sqlalchemy import Column, String, text


class TimestampMixin:
    """
    Defines a SQLAlchemy model mixin that generates and handles timestamp columns. The defaults use PostgreSQL's built-in `now()` function.


    Example
    -------
    `class MySQLAlchemyModel(TimestampMixin):`


    Attributes
    ----------
    `created_at` (string):
    Timestamp referencing when the row was created

    `last_updated` (string):
    Timestamp referencing when the row was last updated


    References
    ----------
    [Stack Overflow: SQLAlchemy default DateTime](https://stackoverflow.com/a/33532154/11715889)

    [Stack Overflow: UTC vs ISO format for time](https://stackoverflow.com/a/58848028/11715889)

    [Stack Overflow: Turn postgres date representation into ISO 8601 string](https://stackoverflow.com/a/55387470/11715889)
    """

    created_at = Column("created_at", String, server_default=text("now()"))

    last_updated = Column("last_updated", String, server_default=text("now()"))
