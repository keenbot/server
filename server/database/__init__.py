from .initialize_database import initialize_database
from .functions import DatabaseFunctions

initialize_database()
