from sqlalchemy.orm import scoped_session, sessionmaker
from .engine import engine
Session = scoped_session(sessionmaker(bind=engine))
