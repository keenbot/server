from .engine import engine
from .base import Base
import os


def initialize_database():
    if os.getenv("ENV") == "dev":
        Base.metadata.drop_all(bind=engine)

    Base.metadata.create_all(bind=engine)
