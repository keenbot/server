from flask import Flask, request
import os

# Middleware modules
from .middleware import DataValidatorMiddleware

# Route modules
from .routes import users_router, intents_router, patterns_router, responses_router

# ------------------------------------------------------------------------------

app = Flask(__name__)


# Middleware
# app.wsgi_app = DataValidatorMiddleware(app.wsgi_app)

# Route handlers
app.register_blueprint(users_router, url_prefix="/api/v1/users/")
app.register_blueprint(intents_router, url_prefix="/api/v1/intents/")
app.register_blueprint(patterns_router, url_prefix="/api/v1/patterns/")
app.register_blueprint(responses_router, url_prefix="/api/v1/responses/")


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def catch_all(path):
    return "You want path: %s" % path
