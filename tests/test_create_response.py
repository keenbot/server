from server.utils import create_response
from flask import Response, jsonify
import pytest


def test_create_response_argument_checks():
    """
    Tests the error handling of `create_response()` for invalid arguments.
    """
    print("\n" + "-" * 80)

    print("""  - create_response() should return a TypeError when an argument
    passed into the "status" parameter is not an integer.\n""")

    # Calls function with a valid type for the "status" parameter.
    try:
        response = create_response(status=200)
        assert True
    except:
        assert False

    # Calls function with an invalid type for the "status" parameter.
    try:
        response = create_response(status="200")
    except TypeError:
        assert True
    except:
        assert False

    # --------------------------------------------------------------------------

    print("""  - create_response() should return a ValueError when an argument
    passed into the "status" parameter is not a valid status code.\n""")

    # Calls function with a valid value for the "status" parameter.
    try:
        response = create_response(status=200)
        assert True
    except:
        assert False

    # Calls function with an invalid value for the "status" parameter.
    try:
        response = create_response(status=999)
    except ValueError:
        assert True
    except:
        assert False

    # --------------------------------------------------------------------------

    print("""  - create_response() should return a TypeError when an argument
    passed into the "message" parameter is not None or a string.\n""")

    try:
        response = create_response(status=200, message=None)
        assert True
    except:
        assert False

    try:
        response = create_response(status=200, message="My test message")
        assert True
    except:
        assert False

    try:
        response = create_response(status=200, message=["My test message"])
    except TypeError:
        assert True
    except:
        assert False

    # --------------------------------------------------------------------------

    print("""  - create_response() should return a TypeError when an argument
    passed into the "body" parameter is not a list or dictionary.\n""")

    # Calls function with a list as an argument for the "body" parameter.
    try:
        response = create_response(status=200, body=[])
        assert True
    except:
        assert False

    # # Calls function with a dictionary as an argument for the "body" parameter.
    try:
        response = create_response(status=200, body={})
        assert True
    except:
        assert False

    # # Calls function with a string as an argument for the "body" parameter.
    try:
        response = create_response(status=200, body="My example body")
    except TypeError:
        assert True
    except:
        assert False


# ------------------------------------------------------------------------------


def test_create_response():

    print("Test: Status 200")
    response = create_response(200)
    assert isinstance(response, Response) == True
    assert response.mimetype == "application/json"
    # assert response.data == {"status": 200, "message": "OK"}

    print("Test: Status 200 with message")
    response = create_response(200, "This is a test message")
    assert isinstance(response, Response) == True
    assert response.mimetype == "application/json"
